import React, { Component } from 'react';
import Button from './Button.js';

class EvaluationButton extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick();
  }
  
  render() {
    return (
      <div>
        <Button buttonText="=" onClick={this.handleClick} />
      </div>
    )
  }
}

export default EvaluationButton;