import React, { Component } from 'react';
import Button from './Button.js';

class Numbers extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(number) {
    this.props.onClick(number);
  }

  render() {
    return (
      <div>
        <Button buttonText="60" onClick={() => this.handleClick(60)} />
        <Button buttonText="8" onClick={() => this.handleClick(8)} />
      </div>
    )
  }
}

export default Numbers;