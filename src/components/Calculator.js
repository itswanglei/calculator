import React, { Component } from 'react';
import Numbers from './Numbers.js';
import Operators from './Operators.js';
import ClearButton from './ClearButton.js';
import EvaluationButton from './EvaluationButton.js';

class Calculator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accumulation: 0,
      currentNumber: 0,
      operator: ''
    };
    this.provideNumber = this.provideNumber.bind(this);
    this.calculateAccumulation = this.calculateAccumulation.bind(this);
    this.clearResult = this.clearResult.bind(this);
    this.showResult = this.showResult.bind(this);
    this.setOperationType = this.setOperationType.bind(this);
  }

  provideNumber(number) {
    let accumulation = this.calculateAccumulation(
      this.state.accumulation,
      number
    );
    this.setState({
      accumulation: accumulation,
      currentNumber: number
    });
  }

  calculateAccumulation(acc, cur) {
    switch (this.state.operator) {
      case '+':
        acc = this.state.currentNumber + cur;
        break;
      case '-':
        acc = this.state.currentNumber - cur;
        break;
      case '*':
        acc = this.state.currentNumber * cur;
        break;
      case '/':
        acc = this.state.currentNumber / cur;
        break;
      default:
        break;
    }
    return acc;
  }

  clearResult() {
    this.setState({
      accumulation: 0,
      currentNumber: 0,
      operator: ''
    });
  }

  showResult() {
    if (this.state.operator !== '') {
      this.setState({
        accumulation: 0,
        currentNumber: this.state.accumulation,
        operator: ''
      });
    }
  }

  setOperationType(operator) {
    this.setState({
      operator: operator
    });
  }

  render() {
    return (
      <div>
        <input value={this.state.currentNumber} readOnly />
        <Operators onClick={this.setOperationType} />
        <Numbers onClick={this.provideNumber} />
        <ClearButton onClick={this.clearResult} />
        <EvaluationButton onClick={this.showResult} />
      </div>
    );
  }
}

export default Calculator;
